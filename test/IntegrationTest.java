import com.fasterxml.jackson.databind.JsonNode;
import org.junit.*;

import play.libs.Json;
import play.libs.ws.WS;
import play.libs.ws.WSClient;
import play.libs.ws.WSResponse;
import play.mvc.*;
import play.test.*;

import java.util.concurrent.CompletionStage;

import static org.junit.Assert.assertEquals;
import static play.test.Helpers.*;
import static org.junit.Assert.*;

import static org.fluentlenium.core.filter.FilterConstructor.*;

public class IntegrationTest {

    @Test
    public void countWords() {
        final int testServerPort = play.api.test.Helpers.testServerPort();
        final TestServer server = testServer(testServerPort);
        running(server, () -> {
            try {
                try (WSClient ws = WS.newClient(testServerPort)) {
                    final String text = "Spam, egg, Spam, Spam, bacon and Spam";
                    final JsonNode payload = Json.newObject().set("text", Json.toJson(text));
                    final CompletionStage<WSResponse> completionStage = ws.url("/api/countwords").post(payload);
                    final WSResponse response = completionStage.toCompletableFuture().get();
                    assertEquals(OK, response.getStatus());
                    final JsonNode data = response.asJson().get("data");
                    assertEquals("Must return a correct number of words", 7, data.asInt());
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }
}

# Count Words Service

### Setup
1. Install sbt (Simple Build Tool). See http://www.scala-sbt.org/0.13/docs/Setup.html for installation details
2. After that, use `sbt run` from project's directory and then go to http://localhost:9000 to see the running server

### How to create a Run configuration for Play2 Framework with Auto-Reload for IntelliJ Idea
Select Run/Debug configuration -> + Add new configuration (Alt+Insert) -> Select 'Play 2 App' -> Check 'Enable Auto-Reload' -> OK
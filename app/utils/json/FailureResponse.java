package utils.json;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.lang3.builder.ToStringBuilder;
import play.libs.Json;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.List;

/**
 * JSON API error document wrapper.
 */
public class FailureResponse {

    /**
     * Errors list. Immutable.
     */
    public final List<ErrorObject> errors;

    /**
     * Creates a new FailureResponse instance.
     *
     * @param errorObjects list of JSON API error objects
     */
    public FailureResponse(@Nonnull List<ErrorObject> errorObjects) {
        errors = Collections.unmodifiableList(errorObjects);
    }

    /**
     * Creates a new FailureResponse instance.
     *
     * @param errorObject JSON API error object
     */
    public static FailureResponse valueOf(@Nonnull ErrorObject errorObject) {
        List<ErrorObject> errors = Collections.singletonList(errorObject);
        return new FailureResponse(errors);
    }


    /**
     * Creates a new FailureResponse instance.
     *
     * @param t Throwable that caused an error.
     */
    public static FailureResponse valueOf(@Nonnull Throwable t) {
        List<ErrorObject> errors = Collections.singletonList(ErrorObject.valueOf(t));
        return new FailureResponse(errors);
    }

    /**
     * Creates a new FailureResponse instance.
     *
     * @param message error message.
     */
    public static FailureResponse valueOf(@Nonnull String message) {
        List<ErrorObject> errors = Collections.singletonList(ErrorObject.valueOf(message));
        return new FailureResponse(errors);
    }

    /**
     * Returns JSON representation of the instance.
     *
     * @return JSON object
     */
    public JsonNode toJson() {
        return Json.toJson(this);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("errors", errors)
                .toString();
    }

}

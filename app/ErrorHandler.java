import play.*;
import play.api.OptionalSourceMapper;
import play.api.routing.Router;
import play.http.DefaultHttpErrorHandler;
import play.mvc.Http.*;
import play.mvc.*;
import utils.json.ErrorObject;
import utils.json.FailureResponse;

import javax.inject.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import static play.mvc.Http.Status.FORBIDDEN;
import static play.mvc.Http.Status.INTERNAL_SERVER_ERROR;

@Singleton
public class ErrorHandler extends DefaultHttpErrorHandler {

    private final Logger.ALogger logger = Logger.of(this.getClass());

    @Inject
    public ErrorHandler(Configuration configuration, Environment environment,
                        OptionalSourceMapper sourceMapper, Provider<Router> routes) {
        super(configuration, environment, sourceMapper, routes);
    }

    @Override
    public CompletionStage<Result> onServerError(final Http.RequestHeader request, final java.lang.Throwable exception) {
        logger.error(String.format("Server error. Request: %s", request.toString()), exception);
        final ErrorObject errorObject = new ErrorObject.Builder()
                .withTitle(exception.getMessage())
                .withStatus(String.valueOf(INTERNAL_SERVER_ERROR))
                .build();
        final Result result = Results.internalServerError(FailureResponse.valueOf(errorObject).toJson());
        return CompletableFuture.completedFuture(result);
    }

    @Override
    public CompletionStage<Result> onClientError(final RequestHeader request, final int statusCode, final String message) {
        logger.info(String.format("Client error. Code: %d. Message: %s. Request: %s", statusCode, message, request.toString()));
        final ErrorObject errorObject = new ErrorObject.Builder()
                .withTitle(!message.isEmpty() ? message : "Client error")
                .withStatus(String.valueOf(statusCode))
                .build();
        final Result result = Results.status(statusCode, FailureResponse.valueOf(errorObject).toJson());
        return CompletableFuture.completedFuture(result);
    }
}

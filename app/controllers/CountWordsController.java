package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Result;
import services.CountWordsService;
import utils.json.FailureResponse;
import utils.json.SuccessResponse;

import javax.inject.Inject;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This controller handles requests related to word counting.
 */
public class CountWordsController extends Controller {

    private final Logger.ALogger log;

    private final CountWordsService countWordsService;

    /**
     * State of the count service (active/internal error/forbidden)
     */
    private CountWordsState countWordsState;

    @Inject
    public CountWordsController(final CountWordsService countWordsService) {
        this.log = Logger.of(this.getClass());
        this.countWordsState = new ActiveState();
        this.countWordsService = countWordsService;
    }

    /**
     * Returns the number of words in a text.
     *
     * @return the number of words in text
     */
    public Result countWords() {
        final JsonNode json = request().body().asJson();
        final String text = json.get("text").asText();
        log.info(String.format("Counting the number of words in '%s'", StringUtils.substring(text, 0, 50)));
        return countWordsState.handle(text);
    }

    /**
     * State of the word counter.
     */
    private interface CountWordsState {
        Result handle(String text);
    }

    /**
     * Active state.
     */
    private class ActiveState implements CountWordsState {

        private final int MAX_ATTEMPTS = 10;

        private AtomicInteger counter = new AtomicInteger(0);

        @Override
        public Result handle(String text) {
            if (counter.intValue() < MAX_ATTEMPTS) {
                final int wordsCount = countWordsService.countWords(text);
                counter.incrementAndGet();
                log.info(String.format("Found %d words", wordsCount));
                return ok(SuccessResponse.valueOf(wordsCount).toJson());
            } else {
                countWordsState = new InternalErrorState();
                return countWordsState.handle(text);
            }
        }
    }

    /**
     * Internal Error state.
     */
    private class InternalErrorState implements CountWordsState {

        private final int TIMEOUT = new Random().nextInt(10 * 1000);

        private final long timestamp = System.currentTimeMillis();

        @Override
        public Result handle(String text) {
            if (System.currentTimeMillis() < timestamp + TIMEOUT) {
                log.info("Return status 500");
                return internalServerError(FailureResponse.valueOf("Internal Error").toJson());
            } else {
                countWordsState = new ForbiddenErrorState();
                return countWordsState.handle(text);
            }
        }
    }

    /**
     * Forbidden state.
     */
    private class ForbiddenErrorState implements CountWordsState {

        private final int TIMEOUT = new Random().nextInt(5 * 1000);

        private final long timestamp = System.currentTimeMillis();

        @Override
        public Result handle(String text) {
            if (System.currentTimeMillis() < timestamp + TIMEOUT) {
                log.info("Return status 403");
                return forbidden(FailureResponse.valueOf("Forbidden").toJson());
            } else {
                countWordsState = new ActiveState();
                return countWordsState.handle(text);
            }
        }
    }
}

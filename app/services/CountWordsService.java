package services;

import com.google.inject.Singleton;

import javax.annotation.Nonnull;

@Singleton
public class CountWordsService {

    /**
     * Returns the number of words in text.
     *
     * @param text text to count the number of words in
     * @return the number of words in text
     */
    public int countWords(@Nonnull final String text) {
        return text.split("\\s+").length;
    }
}
